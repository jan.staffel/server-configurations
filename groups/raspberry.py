global groups

groups['raspberry'] = {
    'os': 'raspbian',
    'os_version': (11, 0),
    'bundles': [
        'openssh',
	'wpa_supplicant',
    ],
    'metadata': {
        'users': {
            'staffel': {
                'sudo': True,
                'passwordless_sudo': True,
                'ssh_pubkeys': [
                    'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKvmnvCIWanAh5+4c06yC9yHq5abXAa4A7ugqYUPoEhW Samsung S20 SSH',
		    'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICyKJB/eIevTZ45deMVRw9X+4nnOztnvLPTqpZFYRizU JuiceSSH ',
                    'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDR2U3BpQjMdQUy2dLbJuCrDVb2WIQMQUDJtnd/S9r3MMqT44lBoC0U9bzSn2U8QqwZv5hFwKpaVYCRAihZZTHCmDYWXJ0YcTbTEXHJQnwve/HCh2NmUoOT8ClnAxw2VV/kOeJ64XyT8MERtjEj2KJZYPezFDRlCSvbLYonWmPwrmlr3VuclZX2Eik8ryTAtXIfyX+NRDu6kpmCnZ8v8T0hwM8Bkaejfjz+d5Eed3L8qlEJpidewDavUEdkPxtfLKyHE/I9KZ3hN6juK36Iwqlg3CYmxw3z6kikgZzc51FGtqxyGhyWlJOiKy/rW562FBH/muB7cJWB9r9Qp4MbaGUp Staffel@DESKTOP-A8471FN',
                    'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFHTIzYR7+grhLZ0pdvYiMcD9w+3nQbEiBxLyEa/h0bo bw-deployment'
		],
		'wpa_supplicant': 'wpa_supplicant.conf',
                'shell': '/bin/bash',
            }
        }
    }
}
