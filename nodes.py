from glob import glob
from os.path import join

nodes = {}
for node in glob(join(repo_path, "nodes", "*.py")):
    with open(node, 'r') as f:
        exec(f.read())