global nodes

nodes['raspberry-secondary'] = {
    'groups': [
        'raspberry'
    ],
    'hostname': "raspberry-secondary",
	'bundles': (
		"octoprint",
		"mjpg-streamer",
	),
}