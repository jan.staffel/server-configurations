users = {
    "streamer": {
        "shell": "/bin/bash",
        "home": "/home/streamer",
        "groups": ["video"]
    },
}

directories = {
    "/home/streamer": {
        "mode": "0755",
        "owner": "streamer",
        "group": "streamer",
    },
    "/etc/mjpg-streamer": {
        "mode": "0755",
        "owner": "root",
        "group": "root"
    }
}

files = {
    '/home/streamer/.bashrc': {
        'mode': '0644',
        'owner': 'streamer',
        'group': 'streamer',
        'source': '.bashrc',
        'needs': {'directory:/home/streamer'}
    },
    '/etc/systemd/system/mjpg-streamer.service': {
        'mode': '0755',
        'owner': 'root',
        'group': 'root',
        'source': 'mjpg-streamer.service',
        'tags': ['mjpg-streamer_service']
    },
}

actions = {
    'install_required_packages': {
        'command': 'sudo apt install -y subversion libjpeg62-turbo-dev imagemagick ffmpeg libv4l-dev cmake',
        'expected_return_code': 0,
        'after': {'directory:/home/streamer'}
    },
    'clone_mjpg-streamer': {
        'command': 'cd /tmp && git clone https://github.com/jacksonliam/mjpg-streamer.git',
        'expected_return_code': 0,
        'unless': 'test -d /tmp/mjpg-streamer',
        'after': {'action:install_required_packages'}
    },
    'make_mjpg-streamer': {
        'command': 'cd /tmp/mjpg-streamer/mjpg-streamer-experimental && make',
        'expected_return_code': 0,
        'after': {'action:clone_mjpg-streamer'}
    },
    'move_binary': {
        'command': 'mv /tmp/mjpg-streamer/mjpg-streamer-experimental/mjpg_streamer /usr/bin',
        'expected_return_code': 0,
        'after': {'action:make_mjpg-streamer'}
    },
    'move_inputs': {
        'command': 'mv /tmp/mjpg-streamer/mjpg-streamer-experimental/input_uvc.so /etc/mjpg-streamer',
        'expected_return_code': 0,
        'after': {
            'action:make_mjpg-streamer',
            'directory:/etc/mjpg-streamer'
        },
        'tags': ['mjpg-streamer_service']
    },
    'move_outputs': {
        'command': 'mv /tmp/mjpg-streamer/mjpg-streamer-experimental/output_http.so /etc/mjpg-streamer',
        'expected_return_code': 0,
        'after': {
            'action:make_mjpg-streamer',
            'directory:/etc/mjpg-streamer'
        },
        'tags': ['mjpg-streamer_service']
    },
    'enable_mjpg_service': {
        'command': 'systemctl daemon-reload && systemctl enable mjpg-streamer',
        'expected_return_code': 0,
        'needs': ['action:set_permissions', 'tag:mjpg-streamer_service'],
    },
    'start_mjpg_service': {
        'command': 'systemctl start mjpg-streamer',
        'expected_return_code': 0,
        'needs': ['action:enable_mjpg_service'],
    },
    'cleanup': {
        'command': 'rm -rf /tmp/mjpg-streamer',
        'expected_return_code': 0,
        'needs': ['action:start_mjpg_service'],
    }
}