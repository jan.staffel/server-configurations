# install octoprint.
# https://octoprint.org/download/

users = {
    "octoprint": {
        "shell": "/bin/bash",
        "home": "/home/octoprint",
        'groups': ['tty', 'dialout', 'video']
    },
}

directories = {
    "/home/octoprint": {
        "mode": "0755",
        "owner": "octoprint",
        "group": "octoprint",
    },
}

files = {
    '/etc/systemd/system/octoprint.service': {
        'mode': '0755',
        'owner': 'root',
        'group': 'root',
        'content_type': 'mako',
        'source': 'octoprint.service',
        'tags': ['systemd_unit']
    },
    '/home/octoprint/initial-config.zip': {
        'mode': '0700',
        'owner': 'octoprint',
        'group': 'octoprint',
        'source': 'secrets/octoprint-initial-config.zip',
        'content_type': 'binary'
    },
}

actions = {
    'set_bashrc': {
        'command': 'cp .bashrc /home/octoprint/ && chown octoprint:octoprint /home/octoprint/.bashrc',
        'expected_return_code': 0,
        'unless': 'test -e /home/octoprint/.bashrc',
        'after': {'directory:/home/octoprint'}
    },
    'prepare_venv': {
        'command': 'python3 -m venv /home/octoprint/venv',
        'expected_return_code': 0,
        'unless': "test -e /home/octoprint/venv/bin/activate",
    },
    'configure_octoprint': {
        'command': "sudo su octoprint -c '/home/octoprint/venv/bin/octoprint plugins backup:restore /home/octoprint/initial-config.zip'",
        'expected_return_code': 0,
        'needs': {
            'tag:pip_octoprint',
            'file:/home/octoprint/initial-config.zip'
        },
    },
    'set_chown': {
        'command': 'chown -R octoprint:octoprint /home/octoprint',
        'expected_return_code': 0,
        'needs': {'action:configure_octoprint'},
    },
    'set_permissions': {
        'command': 'chmod -R 0700 /home/octoprint/venv',
        'expected_return_code': 0,
        'needs': {'action:configure_octoprint'},
    },
    'enable_octoprint_service': {
        'command': 'systemctl daemon-reload && systemctl enable octoprint',
        'expected_return_code': 0,
        'needs': ['action:set_permissions', 'tag:systemd_unit'],
    },
    'start_octoprint_service': {
        'command': 'systemctl restart octoprint',
        'expected_return_code': 0,
        'needs': ['action:enable_octoprint_service'],
    }
}

pkg_pip = {
    "/home/octoprint/venv/octoprint": {
        "installed": True,
        'after': {'action:prepare_venv'},
        'tags': ['pip_octoprint']
    },
}
