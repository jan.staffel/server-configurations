#install wpa_supplicant

files = {
	'/etc/wpa_supplicant/wpa_supplicant.conf': {
		'mode': '0600',
		'owner': 'root',
		'group': 'root',
		'content': repo.vault.decrypt_file('wpa_supplicant.conf'),
	},
}
